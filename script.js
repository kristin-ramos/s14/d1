// console.log("Hello World! This is from External JS");


// let brand = "Asus";
// brand = "Mac";

// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);


// console.log(lastName);

	// without let keyword, return will be "NOT DEFINED"

// let firstName;
// console.log(firstName);



	// returns "undefined"


/* Naming Convention

1. Case sensitive

	let color = "pink";
	let Color = "blue";

	console.log(color);
	console.log(color);

2. Variable name must be relevant to its actual value

	let value = "Robin";

	let goofThief = "Robin";

	let bird = "Robin";

3. Camel Case Notation
	let capitalcityofjapan = "Tokyo"
	let capitalCityOfJapan = "Tokyo"


*/


// console.log("She said, "Hello There!", ");
// console.log('She said, "Hello There!", ');

// console.log('Hello, I\'m Joy');


// ESG Updates

// template literals - using backticks ~ ~

// console.log('She said, "Hello there!~ ');

// Constant
// 	- a type of variable that holds value but it Constant

// const PI= 3.14;
// console.log(PI);


// PI=14;
// console.log(PI);

// const boilingPoint=100;
// console.log(boilingPoint);

// captures the original value of the constant

// let country = "Philippines"
// let continent = "Asia"
// let population = "109.6 million"

// console.log(country);
// console.log(continent);
// console.log(population);

/*copied from mam Joy*/

// console.log("Hello World! This is from External JS");

//Writing a comment
	// single line 	ctrl + /
	// multi-line comment 	ctrl + shift + /


/*
	Syntax and Statement

Syntax 
	- set of rules of how codes should be written correctly

Statement
	- set of instructions, ends with semicolon

*/

//alert("Good afternoon");


/*
	Variables
		- a container that holds value
		-using a 
*/

	// let myName = "Joy";
	// console.log(myName);

/* Anatomy 
	
	**Declaration:
		let <variable name>
			- declaration
			- declaring a variable
			- cannot re-declare same variable name under same scope

	Example:
			let car;
			console.log(car);

			let car = "mercedez";
			console.log(car);

	**Initialization

		- initializing a variable with a value
Example:
	let phone = "iPhone";
	console.log(phone);



	**Re-assignment
		- assigning new value to a variable using equal operator.

Example:		
	myName = "Gab";
	console.log(myName);
*/

// Why do you think variable is important?
	// reusable
	
// let brand = "Asus";
// brand = "Mac";

// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
// console.log(brand);
	

//console.log(lastName);
	// without let keyword, return will be "NOT DEFINED"


// let firstName;
// console.log(firstName);
	// returns "UNDEFINED"


/*
	Naming Convention

	1. Case sensitive
		let color = "pink";
		let Color = "blue";

		console.log(color);
		console.log(Color);

	2. Varibale name must be relevant to its actual value

		let value = "Robin";

		let goofThief = "Robin";
		let bird = "Robin";

	3. Camel Case Notation

	let capitalcityofthephilippines = "Manila City";
	
	//this is readable
	let capitalCityOfThePhilippines = "Manila City";

*/
	
	// let year3 = 3;
	//console.log(year3);

	//not accepted by JS:
		// let 3year = 3;
		// console.log(3year);

		// let @year = 3;
		// console.log(@year);

	// let _year = 3;
	// //console.log(_year);

	// let $year = 3;
	//console.log($year);


	//single & double quotes are accepted
	// let flower = "rose";
	// console.log(flower);

	// let flower2 = 'sunflower';
	// console.log(flower2);

//example:

	//console.log("She said, "Hello there!". ");
	// console.log("She said, 'Hello there!'. ");
	// console.log('She said, "Hello there!". ');

	// console.log('Hello, I\'m Joy!');

	//ES6 updates
		//template literals - using backticks ``

	// console.log(`She said, "Hello there!".`);


/*
	Constant
		- a type of variable that holds value but it cannot be changed nor re-assigned
*/

	// const PI = 3.14;
	// console.log(PI);

	/*
		PI = 14;
		console.log(PI);
		- reassignment is not allowed
	*/
	/*
		const boilingPoint;
		console.log(boilingPoint);
		- declaration must have value else it will be error
	*/

/*
	MINI ACTIVITY

		Declare variables called
			country
			continent
			population

		Assign their values according to your own country (population as of 2020)

		Log their values to the console



/*Data Types

1. String
	-sequence of characters
	-they always wrap with quotes or 
	backticks
	-if no quotes or backticks, JS will consider
	it another variable
	-black strings in console

let food = 'sinigang';
let Name = 'Kristin Ramos'

let x ='Hello';
let y ='World';

x=y;
console.log(x);

2. Number
	-whole number (integer) and decimals 
	(float)
	-blue strings in console

// let x = 4;
// let y = 8;

// let result = x+y;
// console.log(result);

let a = '2';
let b = '8';
let c = '3';


/*let result1 = a+b+c; //concatenate (+)
console.log(result1);*/


// let result2 = b-a-c; //operate in (minus)
// console.log(result2);


/*3. Boolan
	-values TRUE or FALSE
	-with linking verb
	-blue strings in console


let isEarly = true;
console.log(isEarly);

let areLate = false;
console.log(areLate);


4. Undefined
	-variable has been declared hoever no value yet
	-empty value

let job;
console.log(job);

5. Null
	-empty value

6. BigInt()
	-large integers more than the datatype can hold

7. Object
	-one variable can hold several diff data types
	-it is wrapped with curly braces
	-has property & value

let user = {firstName: "Dave",
lastName: "Supan",
email: ["ds@mail.com", "dave@gmail.com", 
"dsupan@yahoo.com"],
age:16,
isStudent: true,
spouse: null
}


*/	


/*Special type of OBJECT

1.ARray
-collection of related data
-enclosed with square brackets

let fruits = ["apple", "banana","strawberry"];
let grades = [89,90,92,97,95];


*/

/*typeof Operator
-evaluates what type of data that u are working with
*/

// let animal = "dog";
// let age = 16;
// let isHappy = true;
// let him;
// let spouse = null;
// let admin = {
// 	name: "Admin",
// 	isAdmin: true
// }
// let ave=[83.5,89.6,94.2];

// console.log(typeof animal);
// console.log(typeof age);
// console.log(typeof isHappy);
// console.log(typeof him);
// console.log(typeof spouse);
// console.log(typeof admin);
// console.log(typeof ave);


/* Functions
		-reusable
		-convenient bec it help
*/



// function sayHello() {
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// 	console.log("Hello World");
// }

// 	sayHello();
// 	console.log("________")
// 	sayHello();


/*
	Syntax of a function:

	function <fName>(){
		//statement / code block
	}
	
Anatomy of a function

1. Declaration
	-function keyword
	-function name + parenthesis (verb + noun)
		-parameters inside the parenthesis
	-curly braces
		-determines its codeblock
		-statements are written inside the 
		codeblock

2. Invocation
	-invoke / calls the function
	-by invoking the function, it executes the
	codeblock
	
	-function name + parenthesis
		-arguments inside the parenthesis

	${name} -simplify the value, extract value, 
	perform arithmetic

	ARGUMENT = VALUE OF PARAMETER

	
	Example:
	
	function greeting(name){
		// alert("Hi Kristin");
		console.log(`Hi ${name}!`);
	}

	greeting(`Angelito`);
	greeting(`Jeremy`);


	function getProduct(x, y){
		console.log(`The product of ${x} 
			and ${y} is ${x*y}`);
	}

	getProduct(5,7);
	getProduct(2.2, 1);


	function getSum(x, y, z){
		console.log(x+y+z);
	}

	getSum(1,2,3);

*/


/* Function with return keyword

Example:


function sayName(fName, lName){
	 return `Hi, my name is ${fName} ${lName}`
}

sayName("Kristin", "Ramos");

// first method
console.log(sayName("Kristin", "Ramos"))

// second method
let result = sayName("Jeremy", "Carangan")
console.log(result)

*/

// function checkUser(Name, Age){

// 	return (`Hi, I'm ${Name}
// 	 My age ${Age} + 10 is ${Age + 10}`)

// }

// console.log(checkUser("Kristin", 12));

// function checkUser(Name, Age){

// 	console.log(`Hi, I'm ${Name}. My age ${Age} + 10 is ${Age + 10}`)

// 	return (`Hi, I'm ${Name}. My age ${Age} + 10 is ${Age + 10}`)
// }

// checkUser("Kristin", 12);





